using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace PackProblem.ReactRedux.UI
{
	public class Program
	{
		public static void Main(string[] args)
		{
			CreateWebHostBuilder(args).Build().Run();
		}

		public static IWebHostBuilder CreateWebHostBuilder(string[] args)
		{
			var configurationRoot = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true).Build();
			return WebHost.CreateDefaultBuilder(args).UseKestrel()
				.UseStartup<Startup>().ConfigureKestrel(options =>
					options.Listen(IPAddress.Parse(configurationRoot.GetSection("ListenUrl").Value),
						int.Parse(configurationRoot.GetSection("ListenPort").Value)));
		}
			
	}
}
