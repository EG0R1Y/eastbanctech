import React from 'react';
import { Route } from 'react-router';
import Layout from './components/Layout';
import Home from './components/Home';
import CreateTask from "./components/CreateTask";
import TaskDatas from "./components/TaskDatas";
import TaskDetails from "./components/TaskDetails";


export default () => (
  <Layout>
    <Route exact path='/' component={Home} />
        <Route path='/create' component={CreateTask} />
        <Route path='/fetch-data' component={TaskDatas} />
        <Route path='/detail' component={TaskDetails} />
  </Layout>
);
