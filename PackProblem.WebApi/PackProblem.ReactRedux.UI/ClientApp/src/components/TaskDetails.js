import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { actionCreators } from "../store/TaskDetailsStore";
import { Collapse, Container, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink } from 'reactstrap';
import Button from '@material-ui/core/Button';
class TaskDetails extends Component {
    componentWillMount() {
        
    }
  componentDidMount() {
    // This method is called when the component is first added to the document
      var number = this.props.location.pathname.replace('/detail/', '');
      this.props.requestTasks(number);
      this.props.SignalRRegisterCommands();
  }
    componentWillUnmount() {
        this.props.SignalRUnRegisterCommands();
    }
  render() {
    return (
      <div>
        <h1>Task Details</h1>
        
            { (this.props.taskDetail === undefined ? "InProgress" : renderTaskTable(this.props)) }
      </div>
    );
  }
}

function renderTaskTable(props) {
  return (
    <table className='table table-striped'>
        <thead>
            <tr>
                <th>Paramert Name</th>
                <th>Value</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Number</td>
                <td>{props.taskDetail.number}</td>
            </tr>
            <tr>
                <td>Capacity</td>
                <td>{props.taskDetail.capacity} </td>
            </tr>
            <tr>
                  <td>MaxPrice</td>
                  <td>{props.taskDetail.maxPrice} </td>
            </tr>

            <tr>
                <td>Status</td>
                <td>{props.taskDetail.status == 0 ? "InProgress" : "Completed"} </td>
              </tr>
              {props.taskDetail.status === 1 ? '' : <tr>
                    <td>Percent</td>
                    <td>{props.taskDetail.progressPercent} </td>
                </tr>}
           
            <tr>
                <td>Conditions</td>
                  <td> {props.taskDetail.conditions.map(condition =>
                      <li key={condition.name}>Name {condition.name}
                          <ul>Weight {condition.weight}</ul>
                          <ul>Price {condition.price}</ul>
                      </li>)}
                  </td>
              </tr>
              {props.taskDetail.status === 0 ? '': <tr>
                        <td>Result</td>
                        <td> {props.taskDetail.resultThings.map(condition =>
                        <li key={condition.name}>Name {condition.name}
                    <ul>Weight {condition.weight}</ul>
                    <ul>Price {condition.price}</ul>
                    </li>)}
                            </td>
                    </tr>}
            
        </tbody>
    </table>
      
  );
}



export default connect(
    state => state.taskDetails,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(TaskDetails);
