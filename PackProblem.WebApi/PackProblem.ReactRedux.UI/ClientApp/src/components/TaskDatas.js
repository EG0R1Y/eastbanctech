import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { actionCreators } from "../store/TaskStore";
import { Collapse, Container, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink } from 'reactstrap';
import Button from '@material-ui/core/Button';
class TaskDatas extends Component {
  componentDidMount() {
    // This method is called when the component is first added to the document
      this.props.requestTasks();
      this.props.SignalRRegisterCommands();
  }
  

    componentWillUnmount() {
        this.props.SignalRUnRegisterCommands();
    }
  render() {
    return (
      <div>
        <h1>Task List</h1>
        
        {renderTaskTable(this.props)}
      </div>
    );
  }
}

function renderTaskTable(props) {
  return (
    <table className='table table-striped'>
      <thead>
        <tr>
          <th>Number</th>
          <th>Status</th>
        <th>Cancel</th>
        </tr>
      </thead>
      <tbody>
              {props.tasks.map(task =>
                  <tr key={task.number}>
                      <td><NavLink tag={Link} className="text-dark" to={`/detail/${task.number}`}> { task.number } </NavLink></td>
                      <td>{task.status == 0 ? "InProgress" : "Completed"}</td>
                      <td>{task.status == 0 ? <Button id={task.number} onClick={() => props.requestCancelTask(task.number)}>Cancel</Button> : ""}</td>
          </tr>
        )}
      </tbody>
    </table>
  );
}



export default connect(
    state => state.tasks,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(TaskDatas);
