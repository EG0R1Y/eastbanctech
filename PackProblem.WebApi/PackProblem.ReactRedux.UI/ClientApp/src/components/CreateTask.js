import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { actionCreators } from "../store/CreateTaskStore";
import { Collapse, Container, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink } from 'reactstrap';

import FormControl from "react-bootstrap/lib/FormControl";
import FormGroup from "react-bootstrap/lib/FormGroup";
import HelpBlock from "react-bootstrap/lib/HelpBlock";
import ControlLabel from "react-bootstrap/lib/ControlLabel";
import Form from "react-bootstrap/lib/Form";
import Button from "react-bootstrap/lib/Button";
import Col from "react-bootstrap/lib/Col";
import Row from "react-bootstrap/lib/Row";




class CreateTask extends Component {
   
  componentDidMount() {
    // This method is called when the component is first added to the document
      var number = this.props.location.pathname.replace('/detail/', '');
     // this.props.requestTasks(number);
      
  }
   
    render() {
        this.inputName = 's';
    return (
      <div>
        <h1>Create new task</h1>
        
            {renderForm(this.props) }
      </div>
    );
  }
}



function FieldGroup({ id, label, help, ...props }) {
    return (
    <FormGroup controlId={id}>
            <ControlLabel>{label}</ControlLabel>{'   '}
        <FormControl {...props} />
    {help && <HelpBlock>{help}</HelpBlock>}
    </FormGroup>
);
}

function renderForm(props) {
    function getValidationState(value) {
        const length = value.length;
        if (length > 10) return 'success';
        else if (length > 5) return 'warning';
        else if (length > 0) return 'error';
        return null;
    }
    return (
        <div>
        <div>
      <Form inline>
        <FieldGroup
        id="formControlsCapacity"
        type="number"
        label="Capacity"
                        placeholder="Enter capacity"
    inputRef={ref => { window.capacityRef = ref }}
        />
            </Form>
            </div>
        <br></br>
        <table className='table table-striped'>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Weight</th>
                    <th>Cost</th>
                    <th>Action</th>
                </tr>
            </thead>
        <tbody>
        {props.conditions.map(condition =>
            <tr key={condition.name}>
                            <td>{condition.name}</td>
                            <td>{condition.weight}</td>
                            <td>{condition.price}</td>
                            <td><Button bsStyle="primary" onClick={() => props.removeCondition(condition.name)}>Delete</Button></td>
        </tr>
          )}
        </tbody>
    </table>
        <div>
            <Form>
                <FormGroup controlId="formHorizontalEmail">
                    <Row>
                        <Col componentClass={ControlLabel} sm={1}>
                            Condition
                        </Col>
                        <Col sm={3}>
                                <FormControl inputRef={ref => { window.nameRef = ref }}  type="text" placeholder="Name" />
                        </Col>
                        <Col sm={3}>
                            <FormControl inputRef={ref => {window.weightRef = ref }} type="number" placeholder="Weight" />
                        </Col>
                        <Col sm={3}>
                            <FormControl inputRef={ref => {window.priceRef = ref }} type="number" placeholder="Price" />
                        </Col>
                        <Col sm={2}>
                                <Button bsStyle="primary" onClick={() => props.addCondition(window.nameRef.value, window.weightRef.value, window.priceRef.value)}>Add</Button>
                        </Col>
                    </Row>
                </FormGroup>
        
            </Form>
            </div>
            <Button bsStyle="primary" onClick={() => props.requestCreateTask(window.capacityRef.value)}>Create</Button>
            <div>
    <Form inline>
                    <FormGroup controlId="formInline">
                        <ControlLabel>Create multiple</ControlLabel>
                        <span>&nbsp;&nbsp;&nbsp;  </span>
                        <FormControl inputRef={ref => { window.capacityMultiple = ref }} type="number" placeholder="Capacity" />
                        <span>&nbsp;&nbsp;&nbsp;  </span>
                        <FormControl inputRef={ref => { window.countMultiple = ref }} type="number" placeholder="Count tasks" />
                        <span>&nbsp;&nbsp;&nbsp;  </span>
                        <FormControl inputRef={ref => { window.countConditions = ref }} type="number" placeholder="Count items" />
                        <span>&nbsp;&nbsp;&nbsp;  </span>
                        <Button bsStyle="primary" onClick={() => props.newTasks(window.capacityMultiple.value, window.countMultiple.value, window.countConditions.value)}>Create</Button>
    </FormGroup>
        </Form></div>
        </div>
    );

}



export default connect(
    state => state.conditions,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(CreateTask);
