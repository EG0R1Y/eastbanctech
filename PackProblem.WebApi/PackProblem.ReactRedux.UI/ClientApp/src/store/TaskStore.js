import axios from 'axios';
import * as SignalR from '@aspnet/signalr';

const signalRReceiveChangedStatus = 'RECEIVE_STATUS_LIST_ITEM';
const signalRReceiveCanceled = 'RECEIVE_CANCELED_LIST_ITEM';
const signalUnRegistered = 'SIGNALR_UNREGISTERED_LIST_ITEM';
const requestTaskListType = 'REQUEST_TASK_LIST';
const receiveTaskListType = 'RECEIVE_TASK_LIST';
const initialState = { tasks: [], isLoading: false };
let connection = {};

export const actionCreators = {
    requestTasks: () => async (dispatch, getState) => {
        connection = new SignalR.HubConnectionBuilder().withUrl(window.Config.url + '/taskChanged')
            .configureLogging(SignalR.LogLevel.Information).build();
        if (connection.connection.connectionState === 2) {
            start(0);
        } else {
            connection.off('CanceledTask');
            connection.off('ChangedTask');
            start(0);
        }
    dispatch({ type: requestTaskListType });
        await axios.get(window.Config.url + '/api/PackTask/GetAll')
          .then(res => {
              const tasks = res.data;
              dispatch({ type: receiveTaskListType, tasks });
          });
    },
    requestCancelTask: (number) => async (dispatch, getState) => {
        await axios.put(window.Config.url + '/api/PackTask/'+ number)
            .then(res => {
                const tasks = res.data;
                
            });
    },
    SignalRRegisterCommands: () => (dispatch, getState) => {
        if (connection.connection.connectionState !== 2) {

            start(0);
        };

        connection.on('CanceledTask',
            data => {
                dispatch({ type: signalRReceiveCanceled, number: data });
                console.log("CanceledTask");
            });
        connection.on('ChangedStatus',
            (data) => {
                dispatch({ type: signalRReceiveChangedStatus, data: data });
                console.log("ChangedStatus");
            });


    },
    SignalRUnRegisterCommands: () => (dispatch, getState) => {

        dispatch({ type: signalUnRegistered });
    }
};
async function start(number) {
    try {
        await connection.start();
        console.log('connected');
        connection.invoke("Subscribe", number).catch(err => console.error(err.toString()));
    } catch (err) {
        console.log(err);
        setTimeout(() => start(), 5000);
    }
};
export const reducer = (state, action) => {
  state = state || initialState;

  if (action.type === requestTaskListType) {
    return {
      ...state,
      isLoading: true
    };
  }
    if (action.type === signalRReceiveCanceled) {
        let tasks = state.tasks.filter(task => task.number !== action.number);
        return {
            tasks: tasks,
            isLoading: true
        };
    }
    if (action.type === signalRReceiveChangedStatus) {
        var tasksNew = state.tasks.filter(task => task.number !== action.data.number);
        tasksNew.push({status: action.data.status, number: action.data.number});
        return {
            tasks: tasksNew,
            isLoading: true
        };
    }

  if (action.type === receiveTaskListType) {
    return {
      ...state,
      tasks: action.tasks,
      isLoading: false
    };
    }
    if (action.type === signalUnRegistered) {
        if (connection.connection.connectionState === 1) {
            connection.invoke("Unsubscribe", 0).catch(err => console.error(err.toString()));
            connection.off('ChangedStatus');
            connection.off('CanceledTask');
        };
        return {

            ...state,
            isLoading: true
        };
    }

  return state;
};
