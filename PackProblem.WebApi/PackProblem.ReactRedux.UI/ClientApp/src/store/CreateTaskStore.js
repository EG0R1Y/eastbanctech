import axios from 'axios';


const requestCreateTaskType = 'REQUEST_CREATE_TASK';
const receiveCreateTaskType = 'RECEIVE_CREATE_TASK';
const addConditionType = 'ADD_CONDITIONS';
const addMultipleTasks = 'ADD_TASKS';

const removeConditionType = 'REMOVE_CONDITIONS';

const initialState = { conditions: [], isLoading: false };

export const actionCreators = {
    
    requestCreateTask: (capacity) => async (dispatch, getState) => {
    dispatch({ type: requestCreateTaskType });
        await axios.post(window.Config.url + '/api/PackTask/',
            {
                
                Capacity: capacity,
                ContitionalItems: getState().conditions.conditions
                })
            .then(res => {
                alert('New task number: ' + res.data);
          });
    },
    addCondition: (name, weight, price) => async (dispatch, getState) => {
        const condition = { name: name, weight: weight, price: price };
        
        dispatch({ type: addConditionType, condition });
    },
    newTasks: (capacity, countTask, countConditions) => async (dispatch, getState) => {
        
        dispatch({ type: requestCreateTaskType });
        await axios.post(window.Config.url + '/api/PackTask/PostTasks',
                {
                    Capacity: capacity,
                    Count: countTask,
                    CountItems: countConditions
                })
            .then(res => {
                alert('Tasks was created');
            });
        dispatch({ type: addMultipleTasks });
    },
    removeCondition: (name) => async (dispatch, getState) => {
        dispatch({ type: removeConditionType, name });
    }
};

export const reducer = (state, action) => {
  state = state || initialState;

  if (action.type === requestCreateTaskType) {
    return {
      ...state,
      isLoading: true
    };
  }

  if (action.type === receiveCreateTaskType) {
    return {
      
        conditions: action.conditions,
      isLoading: false
    };
  }
    if (action.type === addConditionType) {
        if (action.condition.name === '' || action.condition.price === '' || action.condition.weight === '') {
            alert('collect all field');
            return {
                ...state
            }
        }
        let conditions = state.conditions.slice();
        if (!conditions.find((c) => {
            if (c.name === action.condition.name) {
                return true;
            }
        })) {
            conditions.push(action.condition);
        } else {
            alert('there is already such an element');
        }

        return {

            conditions: conditions
        };
    }
    if (action.type === removeConditionType) {
        var conditions = state.conditions.filter(f => f.name !== action.name);

        return {
            conditions: conditions
        };
    }
  return state;
};
