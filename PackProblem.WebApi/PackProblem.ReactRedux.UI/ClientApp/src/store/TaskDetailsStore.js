import axios from 'axios';
import * as SignalR from '@aspnet/signalr';


const requestTaskDetailsType = 'REQUEST_TASK_DETAILS';
const receiveTaskDetailsType = 'RECEIVE_TASK_DETAILS';
const signalRReceiveChangedPercent = 'SIGNALR_RECEIVE_CHANGED_PERCENT';
const signalRReceiveComplete = 'SIGNALR_RECEIVE_COMPLETE_TASK';

const signalUnRegistered = 'SIGNALR_UN_REGISTERED';

const initialState = { taskDetail: undefined, isLoading: false };


let connection = {} ;

export const actionCreators = {
  requestTasks: index => async (dispatch, getState) => {
      connection = new SignalR.HubConnectionBuilder().withUrl(window.Config.url+'/taskChanged')
          .configureLogging(SignalR.LogLevel.Information).build();
      if (connection.connection.connectionState === 2) {
          start(index);
      } else {
          connection.off('PercentChanged');
          start(index);
      }
    dispatch({ type: requestTaskDetailsType });

  //  const url = `api/SampleData/WeatherForecasts?startDateIndex=${0}`;
    //const response = await fetch(url);
    //  const tasks = await response.json();
      await axios.get(window.Config.url + '/api/PackTask/' + index)
          .then(res => {
              const taskDetail = res.data;
              dispatch({ type: receiveTaskDetailsType, taskDetail });
          });

     
    },
  SignalRRegisterCommands: (number) => (dispatch, getState) => {
      if (connection.connection.connectionState !== 2) {

          start(number);
      };
      
      connection.on('PercentChanged',
          data => {
              dispatch({ type: signalRReceiveChangedPercent, percent: data });
              console.log("PercentChanged");
          });
      connection.on('ChangedStatus',
          (data) => {
              dispatch({ type: signalRReceiveComplete, data: data });
              console.log("ChangedStatus");
          });
     

  },
  SignalRUnRegisterCommands: () => (dispatch, getState) => {
      
      dispatch({ type: signalUnRegistered });
  }

};
async function start(number) {
    try {
        await connection.start();
        console.log('connected');
        connection.invoke("Subscribe", number).catch(err => console.error(err.toString()));
    } catch (err) {
        console.log(err);
        setTimeout(() => start(), 5000);
    }
};
export const reducer = (state, action) => {
  state = state || initialState;

  if (action.type === requestTaskDetailsType) {
    return {
      ...state,
      isLoading: true
    };
  }

  if (action.type === receiveTaskDetailsType) {
    return {
      
        taskDetail: action.taskDetail,
      isLoading: false
    };
    }
    if (action.type === signalRReceiveChangedPercent) {
        if (state.taskDetail === undefined) {
            console.log('if (state.taskDetail === undefined) {');
            return state;
        }
        let detail = {};
        for (var key in state.taskDetail) {
            detail[key] = state.taskDetail[key];
        }
        console.log(action.percent);
        detail.progressPercent = action.percent;
        return {

            taskDetail: detail,
            isLoading: false
        };
    }
    if (action.type === signalRReceiveComplete) {
        if (state.taskDetail === undefined) {
            console.log('if (state.taskDetail === undefined) {');
            return state;
        }
        
        
        return {

            taskDetail: action.data,
            isLoading: false
        };
    }
    if (action.type === signalUnRegistered) {
        if (connection.connection.connectionState === 1) {
            connection.invoke("Unsubscribe", state.taskDetail.number).catch(err => console.error(err.toString()));
            connection.off('PercentChanged');
        };
        return {

            ...state,
            isLoading: true
        };
    }

  return state;
};
