import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import * as Counter from './Counter';
import * as TaskStore from "./TaskStore";
import * as TaskDetailsStore from "./TaskDetailsStore";
import * as CreateTaskStore from "./CreateTaskStore";




export default function configureStore (history, initialState) {
    const reducers = {
        counter: Counter.reducer,
        tasks: TaskStore.reducer,
        taskDetails: TaskDetailsStore.reducer,
        conditions: CreateTaskStore.reducer
    };

  const middleware = [
    thunk,
    routerMiddleware(history)
  ];

  // In development, use the browser's Redux dev tools extension if installed
  const enhancers = [];
  const isDevelopment = process.env.NODE_ENV === 'development';
  if (isDevelopment && typeof window !== 'undefined' && window.devToolsExtension) {
    enhancers.push(window.devToolsExtension());
  }

  const rootReducer = combineReducers({
    ...reducers,
    routing: routerReducer
  });

  return createStore(
    rootReducer,
    initialState,
    compose(applyMiddleware(...middleware), ...enhancers)
  );
}
