﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Orleans;
using Orleans.Runtime;
using PackProblem.BLL.Impl;
using PackProblem.BLL.Interfaces;
using PackProblem.DAL;
using PackProblem.DAL.Models;
using PackProblem.MsOrleans.GrainInterfaces.IChannels;

namespace PackProblem.MsOrleans.GrainImpl.Impl
{
	public class TaskChannel : Grain, ITaskChannel, IRemindable
	{
		private HubConnection _connection;
		private IGrainReminder _reminder;
		private bool _isComplete;
		private IDisposable _subscribeCurrentPackDisposable;
		private IDisposable _subscribeBestPackTaskDisposable;
		private string _hubUrl;
		private readonly IPackTaskCalc _packTaskCalc;

		private async void ConnectionStart()
		{
			
			if (_connection == null)
			{
				_connection = new HubConnectionBuilder()
					.WithUrl(_hubUrl + "/taskChanged")
					.Build();
				await _connection.StartAsync();
			}
		}

		public override Task OnDeactivateAsync()
		{
			if (_packTask == null)
			{
				_subscribeBestPackTaskDisposable?.Dispose();
				_subscribeCurrentPackDisposable?.Dispose();
				return base.OnDeactivateAsync();

			}
			if (_packTask.Status != Status.Completed)
			{
				
				Console.WriteLine("DelayDeactivation");
				return OnActivateAsync();
			}
			_subscribeBestPackTaskDisposable?.Dispose();
			_subscribeCurrentPackDisposable?.Dispose();
			return base.OnDeactivateAsync();
		}
		
		public TaskChannel()
		{
			
			//collection age limit
			_packTaskCalc = new PackTaskCalc();
			
			_subscribeCurrentPackDisposable = _packTaskCalc.CurrentPackTaskSubject.DelaySubscription(TimeSpan.FromMilliseconds(1000)).Subscribe(async t =>
			{
				ConnectionStart();
				await _connection.InvokeAsync("PercentChanged", t.percent, _packTask.Number);
				_packTask.PackTaskTemp = t.temp;
				if (_isComplete)
				{
					return;
				}
				using (var db = new ApplicationContext())
				{
					try
					{
						db.Attach(_packTask);
						_packTask.ProgressPercent = t.percent;
						if (_packTask.PackTaskTemp == null)
						{
							_packTask.PackTaskTemp = t.temp;
						}
						else
						{
							_packTask.PackTaskTemp.BestThingEntities = t.temp.BestThingEntities;
							_packTask.PackTaskTemp.Iterate = t.temp.Iterate;
						}
						await db.SaveChangesAsync();
					}
					catch(Exception)
					{
						// ignored
					}
				}

			});
			_subscribeBestPackTaskDisposable = _packTaskCalc.BestPackTaskSubject.Delay(TimeSpan.FromMilliseconds(10)).Where(entity => entity != null)
				.Subscribe(async entity =>
				{
					_isComplete = true;
					_subscribeCurrentPackDisposable?.Dispose();
					int maxPrice;
					
						maxPrice = entity.Sum(item => item.Price);
					
					ConnectionStart();

					await _connection.InvokeAsync("ChangedStatusList", new PackTaskEntity()
					{
						Status = Status.Completed, Number = _packTask.Number
					});

					using (var db = new ApplicationContext())
					{
						_packTask.PackTaskTemp = null;
						var entry = db.Attach(_packTask);
						entry.State = EntityState.Modified;
						_packTask.MaxPrice = maxPrice;
						_packTask.ResultThings = entity;
						_packTask.PackTaskTemp = null;
						_packTask.ProgressPercent = 100;
						_packTask.Status = Status.Completed;
						
						//db.PackTasks.Update(_packTask);
						await db.SaveChangesAsync();
					}
					
					await _connection.InvokeAsync("ChangedStatusItem", _packTask);

				});
		}
		private PackTaskEntity _packTask;


		public override async Task OnActivateAsync()
		{
			var configurationRoot = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true).Build();
			_hubUrl = configurationRoot.GetSection("WebApiIp").Value;
			//_channel = GrainFactory.GetGrain<ITaskChannel>(_packTask.Number);
			if (_packTask != null)
			{
				await base.OnActivateAsync();
			}

			try
			{


				using (var db = new ApplicationContext())
				{
					var id = (int) GrainReference.GetPrimaryKeyLong();
					_packTask = await db.PackTasks.Where(entity => entity.Number == id)
						.Include(entity => entity.Conditions)
						.Include(entity => entity.PackTaskTemp)
						.FirstAsync();
				}
			}
			catch (InvalidOperationException e)
			{
				//Если по какой-то причине зерно удалено из базы, но выполняется попытка его удалить снова
				Console.WriteLine(e);
				ConnectionStart();
				await _connection.InvokeAsync("CanceledTask", (int) GrainReference.GetPrimaryKeyLong());
				throw;
			}

			await base.OnActivateAsync();
		}

		public async Task CreateNewTask(int capacity, List<ItemEntity> conditionItems)
		{
			var id = (int) GrainReference.GetPrimaryKeyLong();
			if (_reminder == null)
			{
				_reminder = await RegisterOrUpdateReminder(_packTask.Number.ToString(), TimeSpan.FromMinutes(9), TimeSpan.FromMinutes(10));
			}
			using (var db = new ApplicationContext())
			{
				try
				{
					
					_packTask = new PackTaskEntity() {Capacity = capacity, Conditions = conditionItems, Number = id};
					db.PackTasks.Update(_packTask);
				

					await db.SaveChangesAsync();
				}
				catch (Exception e)
				{
					Console.WriteLine(e);
				}
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
				_packTaskCalc.StartTask(_packTask);
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
			}
		}



		public async Task CancelTask()
		{
			try
			{
				if (_packTask != null && _packTask.Status != Status.Completed)
				{
					var number = _packTask.Number;
					ConnectionStart();
					await _connection.InvokeAsync("CanceledTask", number);
					_packTaskCalc.CancelTask();
					using (var db = new ApplicationContext())
					{
						
						db.PackTasks.Remove(_packTask);
						db.SaveChanges();
						_packTask = null;
						DeactivateOnIdle();
					}
				}

			}
			catch (Exception e)
			{
				Console.WriteLine(e);
			}

			
		}

		public async Task ResumeTask()
		{
			
			await GetTask();
			if (_reminder == null)
			{
				_reminder = await RegisterOrUpdateReminder(_packTask.Number.ToString(), TimeSpan.FromMinutes(9), TimeSpan.FromMinutes(9));
			}
			
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
			_packTaskCalc.ResumeTask(_packTask);
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
			
		}

		public async Task<PackTaskEntity> GetTask()
		{
			if (_packTask == null)
			{
				var id = (int) GrainReference.GetPrimaryKeyLong();
				using (var db = new ApplicationContext())
				{
					_packTask = await db.PackTasks.Where(entity => entity.Number == id)
						.Include(entity => entity.Conditions).FirstAsync();
				}
			}

			return _packTask;
		}


		public Task ReceiveReminder(string reminderName, TickStatus status)
		{
			
			if (_packTask.Status != Status.InProgress)
			{
				UnregisterReminder(_reminder);
			}
			else
			{
				Console.WriteLine("Thanks for reminding me-- I almost forgot!");
			}
			return Task.CompletedTask;
		}
	}
}