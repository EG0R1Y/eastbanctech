﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Orleans;
using PackProblem.DAL;
using PackProblem.DAL.Models;
using PackProblem.MsOrleans.GrainInterfaces.IChannels;

namespace PackProblem.MsOrleans.GrainImpl.Impl
{
	public class GetListChannel : Grain, IGetListChannel
	{
		public GetListChannel()
		{

		}

		public override Task OnActivateAsync()
		{
			return base.OnActivateAsync();
		}

		private List<ItemEntity> _things;
		private List<PackTaskEntity> _packTasks;
		public async Task<List<PackTaskEntity>> GetAllPackTask()
		{
			
			using (var db = new ApplicationContext())
			{
				_packTasks = await db.PackTasks.OrderByDescending(entity => entity.Number).ToListAsync();
				return _packTasks;
			}
		}

		public async Task<List<PackTaskEntity>> GetNotCompletedPackTask()
		{
			using (var db = new ApplicationContext())
			{
				var packTasks = await db.PackTasks.Where(entity => entity.Status == Status.InProgress).ToListAsync();
				return packTasks;
			}
		}

		public async Task<List<ItemEntity>> GetThing()
		{
			if (_things != null)
			{
				return _things;
			}
			using (var db = new ApplicationContext())
			{
				_things = await db.Items.ToListAsync();
				return _things;
			}
		}
	}
}