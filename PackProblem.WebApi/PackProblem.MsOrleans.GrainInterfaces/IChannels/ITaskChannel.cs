﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Orleans;
using PackProblem.DAL.Models;

namespace PackProblem.MsOrleans.GrainInterfaces.IChannels
{
	/// <summary>
	/// интерфейс зерна задачи, выполняет действия с 1 задачей
	/// </summary>
	public interface ITaskChannel : IGrainWithIntegerKey
	{
		Task CreateNewTask( int capacity, List<ItemEntity> conditionsItems);
		Task CancelTask();
		Task ResumeTask();
		Task<PackTaskEntity> GetTask();
	}
}