﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Orleans;
using PackProblem.DAL.Models;

namespace PackProblem.MsOrleans.GrainInterfaces.IChannels
{
	/// <summary>
	/// Работа со списком задач
	/// </summary>
	public interface IGetListChannel : IGrainWithStringKey
	{
		Task<List<PackTaskEntity>> GetAllPackTask();
		Task<List<PackTaskEntity>> GetNotCompletedPackTask();
	}
}