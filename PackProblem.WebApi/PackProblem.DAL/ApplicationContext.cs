﻿
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using PackProblem.DAL.Models;

namespace PackProblem.DAL
{
	public sealed class ApplicationContext : DbContext
	{
		public DbSet<ItemEntity> Items { get; set; }
		public DbSet<PackTaskEntity> PackTasks { get; set; }


		public ApplicationContext()
		{
			

			Database.EnsureCreated();
		}
		public ApplicationContext(DbContextOptions<ApplicationContext> options)
			: base(options)
		{
			Database.EnsureCreated();
		}
		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			var configurationRoot = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true).Build();
			var connectionString = configurationRoot.GetSection("ConnectionString").Value;
			optionsBuilder.UseSqlServer(connectionString, b => b.MigrationsAssembly("PackProblem.WebApi").EnableRetryOnFailure());
		}
		
	}
}
