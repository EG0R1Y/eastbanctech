﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PackProblem.DAL.Models
{
	public class ItemEntity
	{
		/// <summary>
		/// Идентификатор
		/// </summary>

		[Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]

		public int Id { get; set; }
		/// <summary>
		/// Наименование
		/// </summary>
		public string Name { get; set; }
		/// <summary>
		/// Вес
		/// </summary>
		public int Weight { get; set; }
		/// <summary>
		/// Цена
		/// </summary>
		public int Price { get; set; }
		
	}
}