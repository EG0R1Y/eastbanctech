﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace PackProblem.DAL.Models
{
	/// <summary>
	/// Задача
	/// </summary>
	public class PackTaskEntity
	{
		/// <summary>
		/// Номер задачи, уникальный
		/// </summary>
		[Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Number { get; set; }
		/// <summary>
		/// Вместимость рюкзака
		/// </summary>
		public int Capacity { get; set; }
		/// <summary>
		/// максимальная ценность
		/// </summary>
		public int MaxPrice { get; set; }
		/// <summary>
		/// Статус задачи
		/// </summary>
		public Status Status { get; set; }
		/// <summary>
		/// Прогресс в процентах
		/// </summary>
		public int ProgressPercent { get; set; }
		/// <summary>
		/// Условия задачи(набор предметов)
		/// </summary>
		public ICollection<ItemEntity> Conditions { get; set; }
		/// <summary>
		/// Итоговый набор предметов
		/// </summary>
		public ICollection<ItemEntity> ResultThings { get; set; }
		
		public virtual PackTaskTempEntity PackTaskTemp { get; set; }
		
		
	}

	public enum Status
	{
		InProgress = 0, Completed = 1
	}
}
