﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PackProblem.DAL.Models
{
	/// <summary>
	/// Временное состояние решения задачи
	/// </summary>
	public class PackTaskTempEntity
	{
		[Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }
		/// <summary>
		/// лучший набор предметов на текущем шаге
		/// </summary>
		public List<ItemEntity> BestThingEntities { get; set; }
		/// <summary>
		/// Номер текуего шага
		/// </summary>
		public long Iterate { get; set; }
	}
}