﻿using System.Collections.Generic;
using System.Reactive.Subjects;
using System.Threading;
using System.Threading.Tasks;
using PackProblem.DAL.Models;

namespace PackProblem.BLL.Interfaces
{
	public interface IPackTaskCalc
	{

		Subject<(PackTaskTempEntity temp, int percent)> CurrentPackTaskSubject { get; set; }
		Subject<List<ItemEntity>> BestPackTaskSubject { get; set; }

		CancellationToken CancellationToken { get; set; }
		/// <summary>
		/// Стартует задачу
		/// </summary>
		Task StartTask(PackTaskEntity packTask);
		/// <summary>
		/// Отменяет задачу
		/// </summary>
		void CancelTask();

		Task ResumeTask(PackTaskEntity packTask);
	}
}