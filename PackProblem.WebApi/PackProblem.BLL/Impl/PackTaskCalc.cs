﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using System.Threading;
using System.Threading.Tasks;
using PackProblem.BLL.Interfaces;
using PackProblem.DAL.Models;

namespace PackProblem.BLL.Impl
{
	public class PackTaskCalc : IPackTaskCalc
	{
		private List<ItemEntity> _bestItems;
		public Subject<int> PercentSubject { get; set; }
		public Subject<bool> TaskWasCanceled { get; set; }
		public Subject<(PackTaskTempEntity temp, int percent)> CurrentPackTaskSubject { get; set; }
		public Subject<List<ItemEntity>> BestPackTaskSubject { get; set; }
		private PackTaskEntity _packTask;
		private int _bestPrice;
		public CancellationToken CancellationToken { get; set; }
		private CancellationTokenSource _cancellationTokenSource;

		public PackTaskCalc()
		{
			PercentSubject = new Subject<int>();
			BestPackTaskSubject = new Subject<List<ItemEntity>>();
			CurrentPackTaskSubject = new Subject<(PackTaskTempEntity temp, int percent)>();
			TaskWasCanceled = new Subject<bool>();
		}
		private void CheckSet(List<ItemEntity> items)
		{
			if (_bestItems == null)
			{
				if (items.Sum(item => item.Weight) <= _packTask.Capacity)
				{
					_bestItems = items;
					_bestPrice = items.Sum(item => item.Price);
				}
			}
			else
			{
				if (items.Sum(item => item.Weight) <= _packTask.Capacity && items.Sum(item => item.Price) > _bestPrice)
				{
					_bestItems = items;
					_bestPrice = items.Sum(item => item.Price);
				}
				
			}
		}
	
		private async Task MakeAllSetsWithResume(List<ItemEntity> items, PackTaskTempEntity packTaskTemp)
		{
			var variants = (long)Math.Pow(2, items.Count);
			
			int oldPercent = 0;
			PercentSubject.OnNext(oldPercent);
			long iterate;
			if (packTaskTemp?.BestThingEntities != null && packTaskTemp.Iterate > 0)
			{
				_bestItems = packTaskTemp.BestThingEntities;
				iterate = packTaskTemp.Iterate;
			}
			else
			{
				_bestItems = null;
				iterate = 0;
			}
			long oldIterate = iterate;
			for (var i = iterate; i < variants; i++)
			{
				var i1 = i;
				if (CancellationToken.IsCancellationRequested)
				{
					return;
				}
				await Task.Run(  () =>
				{
					if (CancellationToken.IsCancellationRequested)
					{
						return;
					}
					var newSet = new List<ItemEntity>();
					var bytes = BitConverter.GetBytes(i1);

					for (int j = 0; j < items.Count; j++)
					{
						var b = ((bytes[j / 8] >> j % 8) & 1) != 0;
						if (b)
						{
							if (newSet.Sum(item => item.Weight) + items[j].Weight <= _packTask.Capacity)
							{
								newSet.Add(items[j]);
							}
							else
							{
								CheckSet(newSet);
								break;
							}
						}
					}
					if ((int) ((i1 * 100) / variants) > oldPercent && i1 != variants - 1 ||
					    i1 - oldIterate > 20000 && i1 != variants)
					{
						oldIterate = i1;
						oldPercent = (int) ((i1 * 100) / variants);
						PercentSubject.OnNext(oldPercent);
						CurrentPackTaskSubject.OnNext((new PackTaskTempEntity()
						{
							BestThingEntities = _bestItems,
							Iterate = i1
						}, oldPercent));
					}

					CheckSet(newSet);
				}, CancellationToken);
			}
			if (_bestItems == null)
			{
				_bestItems = new List<ItemEntity>();
			}
			BestPackTaskSubject.OnNext(_bestItems);
		}
		/// <summary>
		/// новая задача 
		/// </summary>
		/// <param name="packTask"></param>
		/// <returns></returns>
		public async Task StartTask(PackTaskEntity packTask)
		{
			_cancellationTokenSource = new CancellationTokenSource();
			CancellationToken = _cancellationTokenSource.Token;
			_packTask = packTask;
			await MakeAllSetsWithResume(packTask.Conditions.ToList(), packTask.PackTaskTemp);
		}

		public void CancelTask()
		{
			_cancellationTokenSource.Cancel();
		}
		/// <summary>
		/// продолжает задачу
		/// </summary>
		/// <param name="packTask"></param>
		/// <returns></returns>
		public async Task ResumeTask(PackTaskEntity packTask)
		{
			_cancellationTokenSource = new CancellationTokenSource();
			CancellationToken = _cancellationTokenSource.Token;
			_packTask = packTask;
			await MakeAllSetsWithResume(packTask.Conditions.ToList(), packTask.PackTaskTemp);
		}
	}
}
