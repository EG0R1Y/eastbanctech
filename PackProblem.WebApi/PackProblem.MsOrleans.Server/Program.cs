﻿using System;
using System.IO;
using System.Net;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Configuration;
using Orleans.Hosting;
using Orleans.Logging;
using PackProblem.MsOrleans.GrainImpl.Impl;

namespace PackProblem.MsOrleans.Server
{
	class Program
	{
		
		// ReSharper disable once UnusedParameter.Local
		static void Main(string[] args)
		{

			var configurationBuilder = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
			var configurationRoot = configurationBuilder.Build();
			
			//var invariant = configurationRoot.GetSection("invariant"); // for Microsoft SQL Server
			//Строка подключения к базе, необходимой для работы нескольких серверов
		//	var connectionString = configurationRoot.GetSection("ConnectionStringOrleans"); 

			var builder = new SiloHostBuilder()
				.Configure<ClusterOptions>(options =>
				{
					options.ClusterId = "cluster1";
					options.ServiceId = "service1";
				})
				.Configure<GrainCollectionOptions>(options =>
				{
					// Set the value of CollectionAge to 10 minutes for all grain
					options.CollectionAge = TimeSpan.FromMinutes(double.Parse(configurationRoot.GetSection("CollectionAge").Value));
					
				})
				//для работы по сети
				//.UseAdoNetClustering(options =>
				//{
				//	options.Invariant = invariant.Value;
				//	options.ConnectionString = connectionString.Value;

				//})
				//локальная работа
				.UseLocalhostClustering()
				.Configure<EndpointOptions>(options => options.AdvertisedIPAddress = IPAddress.Loopback)
				.ConfigureApplicationParts(parts =>
					parts.AddApplicationPart(typeof(GetListChannel).Assembly).WithReferences())
				.ConfigureApplicationParts(parts =>
					parts.AddApplicationPart(typeof(TaskChannel).Assembly).WithReferences())
				//Выполняется при старте, обеспечивает запуск задач, которые не были завершены
				.AddStartupTask<CallGrainStartupTask>()
				.UseInMemoryReminderService()
				.ConfigureLogging(loggingBuilder => loggingBuilder.AddFile("Log").SetMinimumLevel(LogLevel.Warning))
				
				.AddMemoryGrainStorage("PubSubStore");
			


			var silo = builder.Build();
			silo.StartAsync().Wait();

			
			
			Console.WriteLine("Press Enter to close.");
			
			Console.ReadLine();

			// shut the silo down after we are done.
			silo.StopAsync().Wait();
		}
	}
}
