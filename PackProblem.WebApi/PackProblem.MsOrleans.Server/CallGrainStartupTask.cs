﻿
using System.Threading;
using System.Threading.Tasks;
using Orleans;
using Orleans.Runtime;
using PackProblem.MsOrleans.GrainImpl.Impl;
using PackProblem.MsOrleans.GrainInterfaces.IChannels;

namespace PackProblem.MsOrleans.Server
{
	/// <summary>
	/// При старте получает списк задач и запускает не завершенные
	/// </summary>
	public class CallGrainStartupTask : IStartupTask
	{
		private readonly IGrainFactory _grainFactory;

		public CallGrainStartupTask(IGrainFactory grainFactory)
		{
			_grainFactory = grainFactory;
		}
		public async Task Execute(CancellationToken cancellationToken)
		{
			var grain = _grainFactory.GetGrain<IGetListChannel>("GetAll");
			var packTaskEntities = await grain.GetNotCompletedPackTask();
			foreach (var packTaskEntity in packTaskEntities)
			{
				var calcGrain = _grainFactory.GetGrain<ITaskChannel>(packTaskEntity.Number);
				calcGrain.ResumeTask();
			}
		}
	}
}
