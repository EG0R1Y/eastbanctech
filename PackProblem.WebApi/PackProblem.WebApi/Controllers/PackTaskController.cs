﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using Orleans;
using PackProblem.DAL;
using PackProblem.DAL.Models;
using PackProblem.MsOrleans.GrainInterfaces.IChannels;
using PackProblem.WebApi.Models;
using Status = PackProblem.DAL.Models.Status;

namespace PackProblem.WebApi.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class PackTaskController : ControllerBase
	{
		private ApplicationContext _applicationContext;
		private IClusterClient _client;
		public PackTaskController(IClusterClient clusterClient, ApplicationContext applicationContext)
		{
			_client = clusterClient;
			_applicationContext = applicationContext;
		}

		[HttpGet("getAll")]
		public async Task<ActionResult<List<TaskMin>>> GetAllTask()
		{
			//throw new NotImplementedException();
			var taskGrain = _client.GetGrain<IGetListChannel>("GetAll");
			var tasks = await taskGrain.GetAllPackTask();

			return tasks.Select(entity => new TaskMin()
			{
				Status = entity.Status == Status.Completed ? Models.Status.Completed : Models.Status.InProgress,
				Number = entity.Number
			}).ToList();
		}

		[HttpGet("{number}")]
		public async Task<ActionResult<PackTaskEntity>> Get(int number)
		{
			//throw new NotImplementedException();
			var taskGrain = _client.GetGrain<ITaskChannel>(number);
			var task = await taskGrain.GetTask();

			return task;
		}

		[HttpPost]
		public async Task<ActionResult> Post([FromBody] CreateTaskRequest createTaskRequest)
		{
			PackTaskEntity packTask;
			
				var entry = await _applicationContext.PackTasks.AddAsync(new PackTaskEntity());
				packTask = entry.Entity;
				await _applicationContext.SaveChangesAsync();
			
			var taskGrain = _client.GetGrain<ITaskChannel>(packTask.Number);
#pragma warning disable CS4014 // Так как этот вызов не ожидается, выполнение существующего метода продолжается до завершения вызова
			taskGrain.CreateNewTask(createTaskRequest.Capacity,
				createTaskRequest.ContitionalItems.Select(item =>
					new ItemEntity()
					{
						Weight = item.Weight,
						Price = item.Price,
						Name = item.Name
					}).ToList());
#pragma warning restore CS4014 // Так как этот вызов не ожидается, выполнение существующего метода продолжается до завершения вызова
			return Ok(packTask.Number);
		}

		[HttpPost("PostTasks")]
		public async Task<ActionResult> PostTasks([FromBody] CreateMultipleTaskRequest createTaskRequest)
		{
			PackTaskEntity packTask;
			Random rnd = new Random();
			for (int i = 0; i < createTaskRequest.Count; i++)
			{
				var entry = await _applicationContext.PackTasks.AddAsync(new PackTaskEntity());
				packTask = entry.Entity;
				await _applicationContext.SaveChangesAsync();
				var taskGrain = _client.GetGrain<ITaskChannel>(packTask.Number);
				var items = new List<ItemEntity>();
				for (int j = 0; j < createTaskRequest.CountItems; j++)
				{
					int weight = rnd.Next(1, createTaskRequest.Capacity/2);
					int price = rnd.Next(0, createTaskRequest.Capacity);
					string name = "name "+ j;
					items.Add(new ItemEntity()
					{
						Price = price,
						Name = name,
						Weight = weight
					});
				}
#pragma warning disable 4014
				taskGrain.CreateNewTask(createTaskRequest.Capacity, items);
#pragma warning restore 4014
			}

			return Ok();
		}

		[HttpPut("{number}")]
		public async Task<ActionResult> Put(int number)
		{
			try
			{
				var taskGrain = _client.GetGrain<ITaskChannel>(number);
				await taskGrain.CancelTask();
				return Ok();
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				return NoContent();
			}
		}
	}
}