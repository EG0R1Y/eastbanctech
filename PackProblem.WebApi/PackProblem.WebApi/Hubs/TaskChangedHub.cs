﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using PackProblem.DAL.Models;
using PackProblem.WebApi.Models;

namespace PackProblem.WebApi.Hubs
{
	public class TaskChangedHub : Hub
	{
		
		public Task PercentChanged(int percent, int number)
		{
			return Clients.Group(number.ToString()).SendAsync("PercentChanged", percent);
		}
		public async Task ChangedStatusItem(PackTaskEntity packTask)
		{

			await Clients.Group(packTask.Number.ToString())
				.SendCoreAsync("ChangedStatus", new object[] {packTask});
		}
		public async Task ChangedStatusList(PackTaskEntity packTask)
		{

			await Clients.Group("0")
				.SendCoreAsync("ChangedStatus", new object[] { packTask });
			//Надо разбираться, почему-то без этого не работает(когда вместе с ChangedStatusItem отправляется)
			await Task.Delay(500);
		}
		public Task CanceledTask(int number)
		{
			return Clients.Group("0").SendCoreAsync("CanceledTask", new object[] {number});
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="groupId">По номеру задачи</param>
		/// <returns></returns>
		public async Task Subscribe(string groupId)
		{
			try
			{
				await Groups.AddToGroupAsync(Context.ConnectionId, groupId);
			}
			catch (Exception e)
			{
				Console.WriteLine($"Не удалось подписаться на получение уведомлений от клиента '{(Context?.ConnectionId ?? "Пусто")}' ", e);
				
			}
		}
		/// <summary>
		///     Отписаться от получения уведомлений
		/// </summary>
		/// <param name="groupId">Идентификатор группы</param>
		/// <returns></returns>
		public async Task Unsubscribe(string groupId)
		{
			try
			{
				await Groups.RemoveFromGroupAsync(Context.ConnectionId, groupId);
			}
			catch (Exception e)
			{
				Console.WriteLine($"Не удалось отписаться от получения уведомлений от клиента '{(Context?.ConnectionId ?? "Пусто")}'", e);
				
			}
		}

	}
}