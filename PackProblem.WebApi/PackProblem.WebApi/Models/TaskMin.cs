﻿namespace PackProblem.WebApi.Models
{
	public class TaskMin
	{
		public int Number { get; set; }
		public Status Status { get; set; }
		
	}
	public enum Status
	{
		InProgress = 0, Completed = 1
	}
}