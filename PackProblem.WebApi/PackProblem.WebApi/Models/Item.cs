﻿namespace PackProblem.WebApi.Models
{
	public class Item
	{
		/// <summary>
		/// Наименование
		/// </summary>
		public string Name { get; set; }
		/// <summary>
		/// Вес
		/// </summary>
		public int Weight { get; set; }
		/// <summary>
		/// Цена
		/// </summary>
		public int Price { get; set; }
	}
}