﻿

using System;
using System.Collections.Generic;

namespace PackProblem.WebApi.Models
{
	[Serializable]
	public class CreateTaskRequest
	{
		public int Capacity { get; set; }
		public List<Item> ContitionalItems { get; set; }
	}
}
