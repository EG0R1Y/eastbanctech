﻿namespace PackProblem.WebApi.Models
{
	public class CreateMultipleTaskRequest
	{
		public int Capacity { get; set; }
		public int Count { get; set; }
		public int CountItems { get; set; }
	}
}