﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Configuration;
using PackProblem.DAL;
using PackProblem.MsOrleans.GrainInterfaces.IChannels;
using PackProblem.WebApi.Hubs;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;

namespace PackProblem.WebApi
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			
			services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
			services.AddSingleton(CreateClusterClient);
			var corsUrl = Configuration.GetSection("SiteUrl").Value;
			services.AddCors(options => options.AddPolicy("CorsPolicy", builder =>
			{
				builder
					.AllowAnyMethod()
					.AllowAnyHeader()
					.WithOrigins(corsUrl)
					.AllowCredentials();
			}));
			services.AddSignalR();
			services.AddDbContext<ApplicationContext>(options => options.UseSqlServer(Configuration.GetSection("ConnectionString").Value));
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			app.UseCors("CorsPolicy");
		
			app.UseMvc();
			app.UseSignalR(routes =>
			{
				routes.MapHub<TaskChangedHub>("/taskChanged");
				
			});
		}
		private IClusterClient CreateClusterClient(IServiceProvider serviceProvider)
		{
			//var invariant = Configuration.GetSection("Invariant"); // for Microsoft SQL Server

			//var connectionString = Configuration.GetSection("ConnectionStringOrleans");

			var client = new ClientBuilder()
				.ConfigureApplicationParts(parts => parts.AddApplicationPart(typeof(ITaskChannel).Assembly).WithReferences())
				.ConfigureApplicationParts(parts => parts.AddApplicationPart(typeof(IGetListChannel).Assembly).WithReferences())

				.Configure<ClusterOptions>(options =>
				{
					options.ClusterId = "cluster1";
					options.ServiceId = "service1";
				})

				.ConfigureLogging(logging => logging.AddConsole().SetMinimumLevel(LogLevel.Warning))
				//.UseAdoNetClustering(options =>
				//{

				//	options.Invariant = invariant.Value;

				//	options.ConnectionString = connectionString.Value;

				//})
				.UseLocalhostClustering()
				.Build();

			client.Connect(RetryFilter).GetAwaiter().GetResult();
			return client;

			async Task<bool> RetryFilter(Exception exception)
			{

				await Task.Delay(TimeSpan.FromSeconds(2));
				return true;
			}
		}
	}
}

