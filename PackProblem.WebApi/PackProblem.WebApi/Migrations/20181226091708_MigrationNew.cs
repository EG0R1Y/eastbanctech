﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PackProblem.WebApi.Migrations
{
    public partial class MigrationNew : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Items_PackTasks_PackTaskEntityConditionsNumber",
                table: "Items");

            migrationBuilder.DropForeignKey(
                name: "FK_Items_PackTasks_PackTaskEntityResultNumber",
                table: "Items");

            migrationBuilder.RenameColumn(
                name: "PackTaskEntityResultNumber",
                table: "Items",
                newName: "PackTaskEntityNumber1");

            migrationBuilder.RenameColumn(
                name: "PackTaskEntityConditionsNumber",
                table: "Items",
                newName: "PackTaskEntityNumber");

            migrationBuilder.RenameIndex(
                name: "IX_Items_PackTaskEntityResultNumber",
                table: "Items",
                newName: "IX_Items_PackTaskEntityNumber1");

            migrationBuilder.RenameIndex(
                name: "IX_Items_PackTaskEntityConditionsNumber",
                table: "Items",
                newName: "IX_Items_PackTaskEntityNumber");

            migrationBuilder.AddForeignKey(
                name: "FK_Items_PackTasks_PackTaskEntityNumber",
                table: "Items",
                column: "PackTaskEntityNumber",
                principalTable: "PackTasks",
                principalColumn: "Number",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Items_PackTasks_PackTaskEntityNumber1",
                table: "Items",
                column: "PackTaskEntityNumber1",
                principalTable: "PackTasks",
                principalColumn: "Number",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Items_PackTasks_PackTaskEntityNumber",
                table: "Items");

            migrationBuilder.DropForeignKey(
                name: "FK_Items_PackTasks_PackTaskEntityNumber1",
                table: "Items");

            migrationBuilder.RenameColumn(
                name: "PackTaskEntityNumber1",
                table: "Items",
                newName: "PackTaskEntityResultNumber");

            migrationBuilder.RenameColumn(
                name: "PackTaskEntityNumber",
                table: "Items",
                newName: "PackTaskEntityConditionsNumber");

            migrationBuilder.RenameIndex(
                name: "IX_Items_PackTaskEntityNumber1",
                table: "Items",
                newName: "IX_Items_PackTaskEntityResultNumber");

            migrationBuilder.RenameIndex(
                name: "IX_Items_PackTaskEntityNumber",
                table: "Items",
                newName: "IX_Items_PackTaskEntityConditionsNumber");

            migrationBuilder.AddForeignKey(
                name: "FK_Items_PackTasks_PackTaskEntityConditionsNumber",
                table: "Items",
                column: "PackTaskEntityConditionsNumber",
                principalTable: "PackTasks",
                principalColumn: "Number",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Items_PackTasks_PackTaskEntityResultNumber",
                table: "Items",
                column: "PackTaskEntityResultNumber",
                principalTable: "PackTasks",
                principalColumn: "Number",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
