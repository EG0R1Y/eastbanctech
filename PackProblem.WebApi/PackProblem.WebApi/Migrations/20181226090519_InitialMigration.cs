﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PackProblem.WebApi.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PackTaskTempEntity",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Iterate = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PackTaskTempEntity", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PackTasks",
                columns: table => new
                {
                    Number = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Capacity = table.Column<int>(nullable: false),
                    MaxPrice = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    ProgressPercent = table.Column<int>(nullable: false),
                    PackTaskTempId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PackTasks", x => x.Number);
                    table.ForeignKey(
                        name: "FK_PackTasks_PackTaskTempEntity_PackTaskTempId",
                        column: x => x.PackTaskTempId,
                        principalTable: "PackTaskTempEntity",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Items",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Weight = table.Column<int>(nullable: false),
                    Price = table.Column<int>(nullable: false),
	                PackTaskEntityConditionsNumber = table.Column<int>(nullable: true),
	                PackTaskEntityResultNumber = table.Column<int>(nullable: true),
                    PackTaskTempEntityId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Items", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Items_PackTasks_PackTaskEntityConditionsNumber",
                        column: x => x.PackTaskEntityConditionsNumber,
                        principalTable: "PackTasks",
                        principalColumn: "Number",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Items_PackTasks_PackTaskEntityResultNumber",
                        column: x => x.PackTaskEntityResultNumber,
                        principalTable: "PackTasks",
                        principalColumn: "Number",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Items_PackTaskTempEntity_PackTaskTempEntityId",
                        column: x => x.PackTaskTempEntityId,
                        principalTable: "PackTaskTempEntity",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Items_PackTaskEntityConditionsNumber",
                table: "Items",
                column: "PackTaskEntityConditionsNumber");

            migrationBuilder.CreateIndex(
                name: "IX_Items_PackTaskEntityResultNumber",
                table: "Items",
                column: "PackTaskEntityResultNumber");

            migrationBuilder.CreateIndex(
                name: "IX_Items_PackTaskTempEntityId",
                table: "Items",
                column: "PackTaskTempEntityId");

            migrationBuilder.CreateIndex(
                name: "IX_PackTasks_PackTaskTempId",
                table: "PackTasks",
                column: "PackTaskTempId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Items");

            migrationBuilder.DropTable(
                name: "PackTasks");

            migrationBuilder.DropTable(
                name: "PackTaskTempEntity");
        }
    }
}
